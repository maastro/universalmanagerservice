# Universal Manager

The UniversalManager automatically schedules all containers within the [MIA framework](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Home) with corresponding configuration, mapping and validation. It is an implementation of the [Manager Plugin](https://bitbucket.org/maastrosdt/manager-plugin).

## Prerequisites ##

- Java 8
- 512 MB RAM
- Modern browser 
   
## Flow ##

- [FileService](https://bitbucket.org/maastrosdt/fileservice) offers new DicomPackages to the manager
- The manager creates a container for this dicomPackage and stores it in the [container service](https://bitbucket.org/maastrosdt/containerservice)
- The manager retrieves configuration from the [configuration service](https://bitbucket.org/maastrosdt/configurationservice) (this can result in a configuration error)
- The manager retrieves mappings from the [mapping service](https://bitbucket.org/maastrosdt/mappingservice) (this can result in a mapping error)
- The manager validates the mappings and modalities (this can result in a validation or mapping error)
- Mappings are saved on the [container service](https://bitbucket.org/maastrosdt/containerservice) (this can result in a mapping error)
- Validated configuration is saved on the [container service](https://bitbucket.org/maastrosdt/containerservice)
- Validated container is saved
- In case the validation fails or an error occurs, the container status will be updated at the [container service](https://bitbucket.org/maastrosdt/containerservice).
 
## Usage ##

 The [swagger interface](http://localhost:8200/swagger-ui.html) will provide a summary of all possible functions and requests (By default only available for the development profile).