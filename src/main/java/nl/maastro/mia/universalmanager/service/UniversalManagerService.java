package nl.maastro.mia.universalmanager.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import nl.maastro.mia.managerplugin.exception.ConfigurationException;
import nl.maastro.mia.managerplugin.exception.MissingModalityException;
import nl.maastro.mia.managerplugin.service.ManagerService;
import nl.maastro.mia.managerplugin.service.ValidationService;
import nl.maastro.mia.managerplugin.service.communication.ContainerService;
import nl.maastro.mia.managerplugin.service.communication.MappingService;
import nl.maastro.mia.managerplugin.service.communication.SchedulerService;
import nl.maastro.mia.managerplugin.web.dto.configuration.ConfigurationDto;
import nl.maastro.mia.managerplugin.web.dto.configuration.ModuleConfigurationDto;
import nl.maastro.mia.managerplugin.web.dto.container.ContainerDto;
import nl.maastro.mia.managerplugin.web.dto.container.DicomPackageDto;
import nl.maastro.mia.managerplugin.web.dto.container.DicomPackagesDto;

@Service("managerservice")
public class UniversalManagerService extends ManagerService {

	private static final Logger logger = Logger.getLogger(UniversalManagerService.class);
	
	@Autowired
	RestTemplate restTemplate;
	
    private ConfigurationService configurationService;
	
	public UniversalManagerService(
	        ContainerService containerService,
	        ConfigurationService configurationService,
	        MappingService mappingService,
	        ValidationService validationService,
	        SchedulerService schedulerService) {
	    
	    super(containerService, mappingService, validationService, schedulerService);
	    this.configurationService = configurationService;
	}
	
	protected List<ContainerDto> createContainers(
	        DicomPackagesDto dicomPackages, 
            String providerId,
            Integer inputPort) {
	    
	    List<ContainerDto> containers = new ArrayList<>();
	    for (DicomPackageDto dicomPackage : dicomPackages.getDicomPackages()){
	        try {
	            ContainerDto container = containerService.createContainer(dicomPackage, providerId, providerId, inputPort);
	            containers.add(container);
	        } catch (Exception e) {
	            logger.error(e);
	        }
	    }
	    return containers;
	}
	
	protected ConfigurationDto getConfiguration(ContainerDto container) throws ConfigurationException {
	    return configurationService.getConfiguration(container);
	}
	
    protected void validateRequiredModalities(ContainerDto container) throws MissingModalityException {
        Set<String> requiredModalities = getRequiredModalities(container.getConfiguration());
        Set<String> availableModalities = container.getDicomPackage().getImages().keySet();
        validationService.validateRequiredModalities(requiredModalities, availableModalities);
    }
    
    private Set<String> getRequiredModalities(ConfigurationDto configuration) {
        Set<String> requiredModalities = new HashSet<>();
        for (ModuleConfigurationDto moduleConfig : configuration.getModulesConfiguration()) {
            requiredModalities.addAll(moduleConfig.getRequiredModalities());
        }
        return requiredModalities;
    }
}