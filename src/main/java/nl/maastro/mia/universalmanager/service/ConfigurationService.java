package nl.maastro.mia.universalmanager.service;

import java.net.URI;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import nl.maastro.mia.managerplugin.exception.ConfigurationException;
import nl.maastro.mia.managerplugin.service.communication.ContainerService;
import nl.maastro.mia.managerplugin.web.dto.configuration.ConfigurationDto;
import nl.maastro.mia.managerplugin.web.dto.container.ContainerDto;

@Service
public class ConfigurationService {
	private static final Logger LOGGER = Logger.getLogger(ConfigurationService.class);

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	ContainerService containerService;

	@Value("${micro.configurationservice:configuration}")
	private String configurationServiceHost;


	public ConfigurationDto getConfiguration(ContainerDto container) throws ConfigurationException{
		LOGGER.info("Get configuration for inputPort: " + container.getInputPort());
		ConfigurationDto configuration = getConfiguration(container.getInputPort());

		if(configuration==null || configuration.getModulesConfiguration() == null || configuration.getModulesConfiguration().isEmpty()){
			throw new ConfigurationException("Retrieval of configuration failed "
                    + "for containerId: " + container.getId() 
                    +" inputPort:"+container.getInputPort());
		}
		
		return configuration;
	}

	private ConfigurationDto getConfiguration(long inputPort) {
		String baseUrl = "http://"+configurationServiceHost+"/api/configuration";
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl);
		builder.queryParam("inputPort", inputPort);

		URI url = builder.build().encode().toUri();
		LOGGER.info("Get configuration: "+url);

		try{
			ResponseEntity<ConfigurationDto> response = restTemplate.getForEntity(url, ConfigurationDto.class);
			if(response.getStatusCode().is2xxSuccessful()){
				return response.getBody();
			}
		}
		catch(Exception e){
			LOGGER.error("Unable to get configuration: "+url, e);
			return null;
		}
		return null;
	}
}
