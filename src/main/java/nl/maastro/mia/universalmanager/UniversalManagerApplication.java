package nl.maastro.mia.universalmanager;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class UniversalManagerApplication {
	private static final Logger LOGGER = Logger.getLogger(UniversalManagerApplication.class);
	private static final String name = UniversalManagerApplication.class.getPackage().getName();
	private static final String version = UniversalManagerApplication.class.getPackage().getImplementationVersion();
	
	public static void main(String[] args) {    	
		SpringApplication.run(UniversalManagerApplication.class, args);
		LOGGER.info("**** VERSION **** : " + name + " " +  version );
	}
}